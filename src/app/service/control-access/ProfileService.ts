import * as express from "express";
import { Data } from "../../databases/Data";
import { ProfileModel } from "../../model/control-access/ProfileModel";
import { typeMsg } from "../../utils/Values";
import { msgGeneric, msgProfile } from "../../utils/MessageTranslate";

class ProfileService{
    async list(req: express.Request, res: express.Response){
       let data: Data = new Data();
       try {
           await data.executeQuery(req, res, ProfileModel);
           res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
    
    async create(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let profile = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!profile.name, msgProfile.nameProfileRequired);
            data.isErrorReturn400(res);
            let profileSearch = await  ProfileModel.findOne({where:{"name": profile.name}});
            data.validAndAddError(profileSearch, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            let result = await  ProfileModel.create(profile);
            data.obj = result;
            data.addMsg(typeMsg.INFO, msgGeneric.recordInsertedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async update(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let profileId = req.params.id;
        let profile = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!profileId, msgGeneric.idNotSet);
            data.validAndAddError(!profile.fantasy, msgProfile.nameProfileRequired);
            data.isErrorReturn400(res);
            let result = await  ProfileModel.findOne({where:{"name": profile.name}});
            console.log(result);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(result && result.id != profileId, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            await ProfileModel.update(profile, {where: {id: profileId}})
            data.addMsg(typeMsg.INFO, msgGeneric.recordAlteredSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async delete(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let profileId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!profileId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  ProfileModel.findById(profileId);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            await ProfileModel.destroy({where: {"id": profileId}});
            data.addMsg(typeMsg.INFO, msgGeneric.recordRemovedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async get(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let profileId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!profileId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  ProfileModel.findOne({"where": {id: profileId}});
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
}
export default new ProfileService();