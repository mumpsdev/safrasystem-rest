import * as express from "express";
import { Data } from "../../databases/Data";
import { typeMsg } from "../../utils/Values";
import { msgGeneric, msgAction, msgCompany } from "../../utils/MessageTranslate";
import Validation from "../../utils/Validation";
import { ActionModel } from "../../model/control-access/ActionModel";
import { CompanyModel } from "../../model/control-access/CompanyModel";

class ActionService {
    async list(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        try {
            await data.executeQuery(req, res, ActionModel);
            res.status(200).json(data);
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async create(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let action = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!action.method, msgAction.methodActionRequired);
            data.validAndAddError(!action.name && !action.url && !action.router, msgAction.nameOrUrlOrRouterActionRequired);
            data.isErrorReturn400(res);
            let resultFilters = await ActionModel.findOne({
                 where: { "name": action.name, "url": action.url,
                    "router": action.router, "method": action.method}
                });
            data.validAndAddError(resultFilters, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            let result = await ActionModel.create(action);
            data.obj = result;
            data.addMsg(typeMsg.INFO, msgGeneric.recordInsertedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async update(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let actionId = req.params.id;
        let action = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!actionId, msgGeneric.idNotSet);
            data.validAndAddError(!action.method, msgAction.methodActionRequired);
            data.validAndAddError(!action.name && !action.url && !action.router, msgAction.nameOrUrlOrRouterActionRequired);
            data.isErrorReturn400(res);
            let resultFilters = await ActionModel.findOne({
                 where: { "name": action.name, "url": action.url,
                    "router": action.router, "method": action.method}
                });
            data.validAndAddError(resultFilters && resultFilters.id != actionId, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            await ActionModel.update(action, {where: {id: actionId}});
            data.addMsg(typeMsg.INFO, msgGeneric.recordAlteredSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async delete(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let actionId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!actionId, msgGeneric.notSetField);
            data.isErrorReturn400(res);
            let result = await ActionModel.findOne({"where": {id: actionId}});
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            await ActionModel.destroy({ where: { "id": actionId } });
            data.addMsg(typeMsg.INFO, msgGeneric.recordRemovedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async get(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let actionId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!actionId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await ActionModel.findOne({"where": {id: actionId}});
            console.log(result);
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
}
export default new ActionService();