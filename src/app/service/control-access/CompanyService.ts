import * as express from "express";
import { Data } from "../../databases/Data";
import { CompanyModel } from "../../model/control-access/CompanyModel";
import { typeMsg } from "../../utils/Values";
import { msgGeneric, msgCompany } from "../../utils/MessageTranslate";
import { BranchModel } from "../../model/control-access/branchModel";

class CompanyService{
    async list(req: express.Request, res: express.Response){
       let data: Data = new Data();
       try {
           await data.executeQuery(req, res, CompanyModel);
           res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
    
    async create(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let company = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!company.fantasy, msgCompany.fantasyCompanyRequired);
            data.validAndAddError(!company.socialReason, msgCompany.socialReasonCompanyRequired);
            data.isErrorReturn400(res);
            let resultFindName = await  CompanyModel.findOne({where:{"socialReason": company.socialReason}});
            data.validAndAddError(resultFindName, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            let result = await  CompanyModel.create(company);
            data.obj = result;
            data.addMsg(typeMsg.INFO, msgGeneric.recordInsertedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async update(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let companyId = req.params.id;
        let company = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!company.fantasy, msgCompany.fantasyCompanyRequired);
            data.validAndAddError(!company.socialReason, msgCompany.socialReasonCompanyRequired);
            data.isErrorReturn400(res);
            let result = await  CompanyModel.findOne({where:{"socialReason": company.socialReason}});
            console.log(result);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(result  && result.id != companyId, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            await CompanyModel.update(company, {where: {id: companyId}})
            data.addMsg(typeMsg.INFO, msgGeneric.recordAlteredSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async delete(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let companyId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!companyId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  CompanyModel.findById(companyId);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            await CompanyModel.destroy({where: {"id": companyId}});
            data.addMsg(typeMsg.INFO, msgGeneric.recordRemovedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async get(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let companyId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!companyId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  CompanyModel.findOne({
                "where": {id: companyId},
                "include":[{model: BranchModel, attributes: ["cnpj", "fantasy", "socialReason"]}]
            });
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
}
export default new CompanyService();