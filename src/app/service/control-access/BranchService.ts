import * as express from "express";
import { Data } from "../../databases/Data";
import { typeMsg } from "../../utils/Values";
import { msgGeneric, msgBranch, msgCompany } from "../../utils/MessageTranslate";
import Validation from "../../utils/Validation";
import { BranchModel } from "../../model/control-access/branchModel";
import { CompanyModel } from "../../model/control-access/CompanyModel";

class BranchService {
    async list(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        try {
            await data.executeQuery(req, res, BranchModel);
            res.status(200).json(data);
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async create(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let branch = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!branch.fantasy, msgBranch.fantasyBranchRequired);
            data.validAndAddError(!branch.socialReason, msgBranch.socialReasonBranchRequired);
            data.isErrorReturn400(res);
            let company = await CompanyModel.findByPrimary(branch.companyId);
            console.log(company);
            console.log(`IsEmpty: `  + Validation.isEmpty(company));
            data.validAndAddError(Validation.isEmpty(company), msgCompany.companyNotFound);
            data.isErrorReturn400(res);
            let resultFindName = await BranchModel.findOne({ where: { "socialReason": branch.socialReason } });
            data.validAndAddError(resultFindName, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            let result = await BranchModel.create(branch);
            data.obj = result;
            data.addMsg(typeMsg.INFO, msgGeneric.recordInsertedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async update(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let branchId = req.params.id;
        let branch = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!branchId, msgGeneric.idNotSet);
            data.validAndAddError(!branch.fantasy, msgBranch.fantasyBranchRequired);
            data.validAndAddError(!branch.socialReason, msgBranch.socialReasonBranchRequired);
            data.isErrorReturn400(res);
            let company = CompanyModel.findById(branch.companyId);
            data.validAndAddError(!company, msgCompany.companyNotFound);
            data.isErrorReturn400(res);
            let resultFindName = await BranchModel.findOne({ where: { "socialReason": branch.socialReason } });
            data.validAndAddError(resultFindName && resultFindName.id != branchId, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            await BranchModel.update(branch, {where: {id: branchId}});
            data.addMsg(typeMsg.INFO, msgGeneric.recordAlteredSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async delete(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let branchId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!branchId, msgGeneric.notSetField);
            data.isErrorReturn400(res);
            let result = await BranchModel.findById(branchId);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            await BranchModel.destroy({ where: { "id": branchId } });
            data.addMsg(typeMsg.INFO, msgGeneric.recordRemovedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async get(req: express.Request, res: express.Response) {
        let data: Data = new Data();
        let branchId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!branchId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await BranchModel.findOne({
                "where": {id: branchId},
                "include": [{model: CompanyModel, "attributes": ["fantasy", "socialReason", "cnpj"]}]
            });
            console.log(result);
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
}
export default new BranchService();