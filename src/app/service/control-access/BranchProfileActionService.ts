import * as express from "express";
import { Data } from "../../databases/Data";
import { typeMsg } from "../../utils/Values";
import { msgGeneric, msgBranch, msgProfile, msgAction} from "../../utils/MessageTranslate";
import { BranchProfileActionModel } from "../../model/control-access/BranchProfileActionModel";
import { ActionModel } from "../../model/control-access/ActionModel";
import { BranchModel } from "../../model/control-access/branchModel";
import { ProfileModel } from "../../model/control-access/ProfileModel";

class BranchbranchProfileActionService{
    async list(req: express.Request, res: express.Response){
       let data: Data = new Data();
       try {
           await data.executeQuery(req, res, BranchProfileActionModel);
           res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
    
    async create(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let branchProfileAction = req.body;
        try {
            //Verificando se os dados foram informados.
            data.validAndAddError(!branchProfileAction.branchId, msgBranch.branchRequired);
            data.validAndAddError(!branchProfileAction.profileId, msgProfile.profileRequired);
            data.validAndAddError(!branchProfileAction.actionId, msgAction.actionRequired);
            data.isErrorReturn400(res);
            let branch = BranchModel.findOne({where:{id: branchProfileAction.branchId}});
            let profile = ProfileModel.findOne({where: {id: branchProfileAction.profileId}});
            let action = ActionModel.findOne({where: {id: branchProfileAction.actionId}});
            //Verificando se as entidades estão no banco
            data.validAndAddError(!branch, msgBranch.branchNotExist);
            data.validAndAddError(!profile, msgProfile.profileNotExist);
            data.validAndAddError(!action, msgAction.actionNotExist);
            data.isErrorReturn400(res);
            let branchProfileActionSearch = await  BranchProfileActionModel.findOne({where:{
                "branchId": branchProfileAction.branchId,
                "profileId": branchProfileAction.profileId,
                "actionId": branchProfileAction.actionId
            }});
            data.validAndAddError(branchProfileActionSearch, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            let result = await  BranchProfileActionModel.create(branchProfileAction);
            data.obj = result;
            data.addMsg(typeMsg.INFO, msgGeneric.recordInsertedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async update(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let branchProfileActionId = req.params.id;
        let branchProfileAction = req.body;
        try {
            data.validAndAddError(!branchProfileActionId, msgGeneric.idNotSet);
            //Verificando se os dados foram informados.
            data.validAndAddError(!branchProfileAction.branchId, msgBranch.branchRequired);
            data.validAndAddError(!branchProfileAction.profileId, msgProfile.profileRequired);
            data.validAndAddError(!branchProfileAction.actionId, msgAction.actionRequired);
            data.isErrorReturn400(res);
            let branch = BranchModel.findOne({where:{id: branchProfileAction.branchId}});
            let profile = ProfileModel.findOne({where: {id: branchProfileAction.profileId}});
            let action = ActionModel.findOne({where: {id: branchProfileAction.actionId}});
            //Verificando se as entidades estão no banco
            data.validAndAddError(!branch, msgBranch.branchNotExist);
            data.validAndAddError(!profile, msgProfile.profileNotExist);
            data.validAndAddError(!action, msgAction.actionNotExist);
            data.isErrorReturn400(res);
            let branchProfileActionSearch = await  BranchProfileActionModel.findOne({where:{
                "branchId": branchProfileAction.branchId,
                "profileId": branchProfileAction.profileId,
                "actionId": branchProfileAction.actionId
            }});
            data.validAndAddError(branchProfileActionSearch && branchProfileActionSearch.id != branchProfileActionId, msgGeneric.recordAlreadyExists);
            data.isErrorReturn400(res);
            await BranchProfileActionModel.update(branchProfileAction, {where: {id: branchProfileActionId}})
            data.addMsg(typeMsg.INFO, msgGeneric.recordAlteredSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async delete(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let branchProfileActionId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!branchProfileActionId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  BranchProfileActionModel.findById(branchProfileActionId);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            await BranchProfileActionModel.destroy({where: {"id": branchProfileActionId}});
            data.addMsg(typeMsg.INFO, msgGeneric.recordRemovedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async get(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let branchProfileActionId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!branchProfileActionId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  BranchProfileActionModel.findOne({
                "where": {id: branchProfileActionId},
                "include": [
                    {model: BranchModel, "attributes": ["fantasy", "socialReason"]},
                    {model: ProfileModel, "attributes": ["name"]},
                    {model: ActionModel, "attributes": ["url", "name", "router", "menuLabel", "group", "icon"]}
                ]
            });
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
}
export default new BranchbranchProfileActionService();