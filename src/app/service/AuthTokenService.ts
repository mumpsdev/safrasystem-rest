import * as express from "express";
import { typeMsg, secretToken } from './../utils/Values';
import * as jwt from "jsonwebtoken";
import { UserModel } from '../model/UserModel';
import { Data } from '../databases/Data';
import { msgAuth, msgGeneric } from '../utils/MessageTranslate';
import UtilService from "./UtilService"

class AuthTokenService{
    private prefixUrl = "/api/v1";
    constructor(){
    }

    public async getLoginAuth(req: express.Request, res: express.Response){
        let data = new Data()
        try {
            let login = req.params["login"]
            let result = await UserModel.findOne({where:{"login": login}})
            let useAuth= {}
            if(!result){
                data.addMsg(typeMsg.DANGER, msgAuth.loginInvalid)
                res.status(401).json(data);
            }else{
                useAuth["login"] = result["login"]
                useAuth["name"] = result["name"]
            }
            data.obj = useAuth
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER,  msgAuth.errorQueryLoginAccess)
        }finally{
            res.status(200).json(data)
        }
    }

    public async authenticate(req: express.Request, res: express.Response){
        let data = new Data()
        //let result: any
        let result: any = {}
        let userActions: Array<string> = [];

        try {
            //let userLogin = req.body;
            let login = req.body.login
            let password = req.body.password
            data.validAndAddError(!login, msgAuth.loginRequired)
            data.validAndAddError(!password, msgAuth.loginRequired)
            data.isErrorReturn400(res);
            let passwordEncript = UtilService.encrypt(password);
            result.user = await UserModel.findOne( 
                { where: { "login" : login, "password": passwordEncript} ,
                attributes: { exclude: [ "dateCreate", "dateUpdate" ]}});
            data.validAndAddError(!result || !result.user, msgAuth.loginPasswordInvalid);
            data.isErrorReturn401(res);
            console.log(`user: ${result.user['password']}`);
            console.log(`password: ${passwordEncript}}`);
            result.user['password'] = '';
            let dataBase = Date.now()
            userActions = data.getActionsUser(result.user);
            let token = jwt.sign(
                {   "iss" : 'https://safrasystem.com.br/', // (Issuer) Origem do token
                    "iat": Math.floor(dataBase), // (issueAt) Timestamp de quando o token foi gerado
                    "exp" : Math.floor(dataBase + ( 1 * 60 * 60 * 1000 ) ), //(Expiration) Timestamp de quando o token expira
                    "sub" : result.user.id, //(Subject) Entidade a quem o token pertence
                    "login": result.user.login,
                    "userActions": userActions
                }, secretToken.SECRET);
            res.append(secretToken.TOKEN, token);
            result[secretToken.TOKEN] = token;
            data.obj = result;
            data.addMsg(typeMsg.SUCCESS, msgAuth.tokenGenerateSuccessfully);
            res.status(200).json(data)
        } catch (error) {
            console.log(error);
            data.addMsg(typeMsg.DANGER, msgAuth.errorQueryLoginAccess)
            res.status(401).json(data)
        }
    }

    public async validateToken(req: express.Request, res: express.Response, next: express.NextFunction){
        let data = new Data()
        try {
            let token = req.body[secretToken.TOKEN] || req.query[secretToken.TOKEN] || req.headers[secretToken.TOKEN]
            console.log(token);
            data.validAndAddError(!token, msgAuth.notSendToken);
            data.isErrorReturn401(res);
            let tokenValid =  await jwt.verify(token, secretToken.SECRET)
            
            // let user = await UserModel.findOne( 
                //     { where: { "login" : userLogged["login"]} ,
                //     attributes: { exclude: [ "password", "dateCreate", "dateUpdate" ] } 
                // })
            let userActions: Array<string> = tokenValid["userActions"]
            let actionValid = data.getUrlPattern(this.prefixUrl, req.originalUrl);
            if(req.method){
                actionValid += "-" + req.method;
            }
            if(req.body && req.body.branchId){
                actionValid += "-" + req.body.branchId;
            }
            data.validAndAddError(userActions.indexOf(actionValid) >= 0, msgAuth.acessDanied);
            data.isErrorReturn401(res);
            req["login"] = tokenValid["login"];
            next();
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgAuth.tokenInvalid);
            data.addMsg(typeMsg.DANGER, error);
            res.status(500).json(data);
            return
        }    
        
    }

    public async getUserLogged(req: express.Request, res: express.Response){
        let data = new Data()
        try {
            let userLogged = req["login"]
            if(userLogged){
                   data.obj = await UserModel.findOne({where:{"login": userLogged.login}})
            }else{
                data.addMsg(typeMsg.DANGER, msgAuth.notLoginFound)
            }
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgAuth.errorQueryLoginAccess)
            res.status(500).json(data)
        } finally {
            res.status(200).json(data)
        }
    }
}

export default new AuthTokenService()