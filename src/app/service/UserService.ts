import * as express from "express";
import { Data } from "../databases/Data";
import { UserModel } from "../model/UserModel";
import { msgGeneric, msgUser } from "../utils/MessageTranslate";
import { typeMsg } from "../utils/Values";
import UtilService from "./UtilService";

class UserService{
    async list(req: express.Request, res: express.Response){
       let data: Data = new Data();
       try {
           await data.executeQuery(req, res, UserModel);
           res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
    
    async create(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let user = req.body;
        try {
            //Validando dados obrigatórios.
            data.validAndAddError(!user.name, msgUser.nameUserRequired);
            data.validAndAddError(!user.login, msgUser.nameUserRequired);
            data.validAndAddError(!user.password, msgUser.nameUserRequired);
            data.isErrorReturn400(res);
            let resultFindName = await  UserModel.findOne({where:{"login": user.login}});
            data.validAndAddError(!resultFindName, msgUser.loginAlreadyRegistered);
            data.isErrorReturn400(res);
            user.password = UtilService.encrypt(user.password);
            let result = await  UserModel.create(user);
            data.obj = result;
            data.addMsg(typeMsg.INFO, msgGeneric.recordInsertedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async update(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let userId = req.params.id;
        let user = req.body;
        try {
            //Validando dados obrigatórios.
            data.validAndAddError(!user.name, msgUser.nameUserRequired);
            data.validAndAddError(!user.login, msgUser.nameUserRequired);
            data.validAndAddError(!user.password, msgUser.nameUserRequired);
            data.isErrorReturn400(res);
            let result = await  UserModel.findOne({where:{"login": user.login}});
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(result &&  result.id != userId, user.loginAlreadyRegistered);
            data.isErrorReturn400(res);
            await UserModel.update(user, {where: {id: userId}})
            data.addMsg(typeMsg.INFO, msgGeneric.recordAlteredSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async delete(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let userId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!userId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  UserModel.findByPrimary(userId);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            await UserModel.destroy({where: {"id": userId}});
            data.addMsg(typeMsg.INFO, msgGeneric.recordRemovedSuccessfully);
            res.status(201).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }

    async get(req: express.Request, res: express.Response){
        let data: Data = new Data();
        let userId = req.params.id;
        try {
            //Verificando se foi passado o id.
            data.validAndAddError(!userId, msgGeneric.idNotSet);
            data.isErrorReturn400(res);
            let result = await  UserModel.findByPrimary(userId);
            //Verificando se existe e se é o mesmo id do que está sendo enviado no request.
            data.validAndAddError(!result, msgGeneric.recordNotExist);
            data.isErrorReturn400(res);
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(typeMsg.DANGER, msgGeneric.errorListRecords);
            res.status(500).json(data);
        }
    }
}
export default new UserService();