import { Sequelize } from "sequelize-typescript";
import { ActionModel } from "../model/control-access/ActionModel";
import { BranchModel } from "../model/control-access/branchModel";
import { BranchProfileActionModel } from "../model/control-access/BranchProfileActionModel";
import { CompanyModel } from "../model/control-access/CompanyModel";
import { ProfileModel } from "../model/control-access/ProfileModel";
import { UserModel } from "../model/UserModel";

export class FactoryDB{
    
  

    private sequelize: Sequelize;

    constructor(){
    }

    createConnectionSequelize(onConnected: any, onError: any){
          //Dados conexão Local.
        let host = "localhost";
        let port = 3306;
        let database = "test";
        let user  = "user_local";
        let password = "";
        let dialect = "postgres"
        let typeSync = "alter"; //Acredito que não funciona legal
        let nodeEnv:string = process.env.NODE_ENV;
        console.log("Node_ENV: " + nodeEnv);
        if(!nodeEnv){
            console.log("---------------Ambiente Local");
        }else if(nodeEnv.indexOf("test") > -1){
            console.log("---------------Ambiente de Teste");
        }else if(nodeEnv.indexOf("development") > -1){
            host = "pellefant.db.elephantsql.com";
            database = "wgrjwaeu";
            port = 5432;
            user = "wgrjwaeu";
            password = "rpYU0d9QQZj9pSBkVigaPbjFlPr7AYI6";
            console.log("---------------Ambiente de Desenvolvimento");
        }else if(nodeEnv.indexOf("production") > -1){
            console.log("---------------Ambiente de produção!");
            host = "mysql785.umbler.com";
            port = 3306;
            password = "iZ*]c3GpC2#z";
        }
        this.sequelize =  new Sequelize({
            host: host,
            port: port,
            database: database,
            dialect: dialect,
            username: user,
            password: password,
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            },
            logging: false
        });
        //Adicionando os modelos que serão espelhados como tabelas no banco de dados.
        this.sequelize.addModels([
            UserModel,
            BranchModel,
            CompanyModel,
            ProfileModel,
            ActionModel,
            BranchProfileActionModel
        ]);
        
        this.sequelize.authenticate().then(onConnected).catch(onError);

    }

    updateTableSequelize(onUpdate: any, onError: any){
        if(this.sequelize){
            // this.sequelize.sync({"force": true}).then(onUpdate).catch(onError);

            // this.sequelize.sync({"alter": true}).then(onUpdate).catch(onError);

            this.sequelize.sync({"logging": (sql) => {console.log(sql);}}).then(onUpdate).catch(onError);
            
            this.sequelize.sync().then(onUpdate).catch(onError);
        }
    }

    getSequelize(): Sequelize {
        return this.sequelize;
    }

    closeConnectionSequelize(onClose: any){
        this.sequelize.close().then(onClose);
    }
}