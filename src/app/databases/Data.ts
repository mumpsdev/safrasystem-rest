import * as express from "express";
import { Sequelize, Model} from 'sequelize-typescript';
import UtilService from '../service/UtilService';
import Validation from "../utils/Validation";
import { typeMsg } from "../utils/Values";

export class Data{
    constructor(
    ){
    }
    public obj: Object;
    public list: Array<any>;
    public listMsg: Array<any>; 
    public links: Array<any>;  
    
    public page: number;  
    public totalPages: number;  
    public limit: number;  
    public rangeInit: number;  
    public rangeEnd: number;  
    public field: string;  
    public qtdTotal: number;  
    public offset: number;
    public contentRange: string; 
    public acceptRange: string;
    private Op : any;
    public query : any;
    private listAnds: Array<any>;
    private listNot: Array<any>;
    private listBetween: Array<any>;
    private listIncludes: Array<any>;
    /**
     * Função que retorna a lista de mensagem criadas, caso esteja nula ela será instanciada.
     */
    private getListMsg(){
        if(!this.listMsg){
            this.listMsg = new Array();
        }
        return this.listMsg;
    }
    /**
     * Função utilizada para adicionar uma mensagem a lista de mensagem que será retornada no response.
     * @param typeMsg O tipo de mensagem que será exibida pelo Client.
     * @param textMsg O texto que será apresentado pelo Client(normalmente envia-se um código que será substituido no Client)
     */
    public addMsg(typeMsg: string, textMsg: string) {
        this.getListMsg().push({type: typeMsg, msg: textMsg});
    }

    // public addMsgWithDetails(typeMsg: string, codMsg: string, textMsg: string,
    //     paramMsg: string, valueMsg: string) {        
    //     this.getListMsg().push({type: typeMsg, cod: codMsg, text: textMsg, param: paramMsg, value: valueMsg});        
    // }
    /**
     * Função que verifica se existem alguma mensagem do tipo DANGER na lista de mensagem.
     */
    public isErrors(): boolean {
        if(this.listMsg && this.listMsg.length > 0){
            return this.listMsg.some( (msg) => msg.type && msg.type == typeMsg.DANGER);
        }
        return false;
    }
    /**
     * Verifica se tem algum erro na expressão e returna o response setando o status para 400 e o data como json.
     * @param res REsponse do express
     */
    public isErrorReturn400(res:express.Response){
        if(this.isErrors()){
           return res.status(400).json(this);
        }
    }
    /**
     * Verifica se tem algum erro na expressão e returna o response setando o status para 401 e o data como json.
     * @param res REsponse do express
     */
    public isErrorReturn401(res:express.Response){
        if(this.isErrors()){
           return res.status(400).json(this);
        }
    }
    /**
     * Verifica se a condição é verdadeira, caso não seja será adicionada uma mensagem com o texto passado por parâmetro.
     * @param validation 
     * @param msgError 
     */
    public validAndAddError(validation: any, msgError: string){
        if(validation){
            this.addMsg(typeMsg.DANGER, msgError);
        }
    }
    /**
     * Função que faz a preparação e criação dos atributos para o filtro e caso nescessário a paginação
     *  e também faz execução da query.
     * @param req Objeto de requisição.
     * @param res Objeto de resposta
     * @param model O model que será responsável de identifcar qual objeto será utilizado na query.
     */
    public async executeQuery(req:express.Request, res:express.Response, model: any){
        try{
            //Palavras que nâo entrarão no where.
            let notWhere: Array<string> = ["page", "limit", "asc", "desc", "range", "fields", "notin"];
            this.Op = Sequelize.Op;
            let LIMIT_PAGE = 10;
            if(!this.query){
                this.query = {};
            }
            let asc = req.query.asc && req.query.asc.indexOf(",") > -1 ? req.query.asc.split(',') : req.query.asc;
            let desc = req.query.desc && req.query.desc.indexOf(",") > -1 ? req.query.desc.split(',') : req.query.desc;
            let fields = req.query.fields && req.query.fields.indexOf(",") > -1 ? req.query.fields.split(',') : req.query.fields;
            this.page = req.query.page ? req.query.page : undefined;
            this.limit = (req.query.limit || this.page) ? req.query.limit || LIMIT_PAGE : undefined;
            if(this.page && this.limit){
                this.offset = (this.page -1) * this.limit;
            }
            let orderParams = UtilService.getCriteriosOrdenacao(asc, desc, "id");
            //Percorrendo todos os parâmetros e adicionando no where.
            for(let key in req.query){
                if(req.query[key] && notWhere.indexOf(key) < 0){
                    this.addAND(key, req.query[key]);
                }
            }
            //Verificando se foi passado o parâmetro fields e limitando os campos que serão retornados.
            if(fields){
                fields = [].concat(fields);
                this.setFields(fields);
            }
            if(orderParams){
                this.query["order"] = orderParams;
            }
            if(this.limit){
                this.query["limit"] = typeof this.limit == "string" ? parseInt(this.limit)  : this.limit;
            }
            if(this.limit){
                this.query["offset"] = this.offset;
            }
            if(this.listIncludes && this.listIncludes.length > 0){
                this.query["include"] = this.listIncludes;
            }//Se caso exista os ands
            let where = [];
            if(this.listAnds && this.listAnds.length > 0){
                where.push({[this.Op.and] : this.listAnds});
            }
            if(this.listNot && this.listNot.length){
                where.push({[this.Op.not] : this.listNot});
            }
            // where.push({[this.Op.between] : [{id: [1,3]}]});
            if(where && where.length > 0){
                this.query["where"] = where;
            }
            let result = await model.findAndCountAll(this.query);
            
            this.qtdTotal = result.count;
            if(this.page && this.limit){//Verificando se está usando paginação e criando os rangeInit e rangeEnd
                this.calcRanges();
            }
            this.list = result.rows;
            //Limpando dados que não precisam está no objeto..
            this.listAnds = undefined; this.Op = undefined; this.query = undefined;
            this.listIncludes = undefined, this.listNot = undefined;
        }catch(error){
            console.log(error);
        }
    }
    /**
     * Função que calcula o intervalo de registro retornado pela query executada.
     */
    private calcRanges(){
        if(this.page > 0 && this.qtdTotal > 0){
            this.totalPages = Math.ceil(this.qtdTotal / this.limit);
        }
        if(this.page < this.totalPages){
            this.rangeEnd = this.page * this.limit;
        } else {
            this.rangeEnd = this.qtdTotal;
            this.page = this.totalPages;
        }
        if(this.page != this.totalPages){
            this.rangeInit = (this.rangeEnd + 1) - this.limit 
        } else{
            this.rangeInit =  ((this.totalPages - 1) * this.limit) + 1;
        }
    }
    /**
     * Função que verifica se o campo é do tipo número, data ou se não contem o caracter "%",
     *  caso ele seja do tipo string e tiver o "%" será adicionada um like.
     * @param field Nome do campo será criado para pesquisa.
     * @param value Valor do campo será criado para pesquisa.
     */
    private setObjectSearch(field: string, value: any){
        let obj = {};
        if(Validation.isOnlyNumbers(value) || Validation.isDateBrVallid(value) || value.indexOf("%") < 0){
            
            obj[field] = value;
        }else{
            obj[field] = {[this.Op.iLike]: value};
        }
        return obj;
    }

    /**
     * Função que criar os campos que serão retornadas no objeto.
     * @param fields lista com nomes dos campos.
     */
    public setFields(fields: Array<string>){
        if(!this.query){
            this.query = {};
        }
        this.query["attributes"] = fields;
    }
    /**
     * Função que faz um Join do objeto que será executada a query com outro objeto.
     * @param model Modelo que será realizado o join.
     * @param collumn qual campo do objeto da query que será utilizada no join.
     * @param foreignkey qual campo do objeto que será feito o join.
     * @param atributes Os atributos referente ao Modelo do join que serão retornados.
     */
    public addInclude(model: any, collumn?: string, foreignkey?: string, atributes?: string[]): any{
        if(!this.listIncludes){
            this.listIncludes = new Array();
        }
        let include = {};
        let where = {};
        include["model"] = model;
        if(collumn && foreignkey){
            where[collumn] = Sequelize.col(foreignkey);
            include["where"] = where;
        }
        if(atributes && atributes.length > 0){
            include["attributes"] = atributes;
        }
        this.listIncludes.push(include);
        return include;
    }
    /**
     * Criar condição AND no Objeto que será executada a query.
     * @param field Nome do campo.
     * @param value Valor do campo.
     */
    public addAND(field: string, value: any){
        if(value){
            if(!this.listAnds){
                this.listAnds = new Array();
            }
            this.listAnds.push(
                this.setObjectSearch(field, value)
            );
        }
    }

    /**
     * Criar condição NOT AND no Objeto que será executada a query.
     * @param field Nome do campo.
     * @param value Valor do campo.
     */
    public addNotIn(field: string, value: Array<any>){
        if(value != undefined){
            let obj = {};
            if(!this.listNot){
                this.listNot = new Array();
            }
            obj[field] =  value;
            this.listNot.push(obj);
        }
    }

    /**
     * Criar condição NOT LIKE no Objeto que será executada a query.
     * @param field Nome do campo.
     * @param value Valor do campo.
     */
    public addNotLike(field: string, value: string){
        if(value != undefined){
            let obj = {};
            if(!this.listAnds){
                this.listAnds = new Array();
            }
            obj[field] = {[this.Op.notLike]: value};
            this.listAnds.push(obj);
        }
    }

    /**
     * Retorna a url no formado original do express sem os parâmetros.
     * @param versao A primeira parte utilizada na url.
     * @param urlOriginal A url direta do request com todos os parâmetros.
     */
    public getUrlPattern(versao: string, urlOriginal: string){
        let urlSemVersao = (urlOriginal.indexOf("?") > -1)
              ? urlOriginal.substring(versao.length + 1, urlOriginal.indexOf("?"))
              : urlOriginal.substring(versao.length + 1);
        let dominio = (urlSemVersao.indexOf("/") > -1) 
              ? urlSemVersao.substring(0, urlSemVersao.indexOf("/"))
              : urlSemVersao;
        let parametros = urlSemVersao.substring(dominio.length+1);
        let partes = parametros.split("/");
        parametros = "";
        if(partes && partes.length > 0){
            let valor = partes[0];
            if(valor && partes.length == 1){
                if(Validation.isOnlyNumbers(valor)){
                    parametros += "/:id"; 
                }else{
                    parametros += "/" + valor; 
                }
            }else{
                partes.forEach((p, index) =>{
                if(index > 0 && (index +1) % 2 == 0){
                    let campo = partes[index-1];
                    parametros +=  "/" + campo + "/:" + campo; 
                }
                });
          }
        }
        return versao + "/" + dominio + parametros;
    }

    public getActionsUser(user: any): Array<string>{
        let userActions: Array<string> = [];
        let profile = user["profile"];
        let branchProfileActions = profile["BranchProfileActions"];
        if(branchProfileActions){
            branchProfileActions.forEach(branchProfileAction => {
                let urlTypeBranch = "";
                let action = branchProfileAction["action"];
                let branch  = branchProfileAction["branch"]
                if(action){
                    urlTypeBranch = action.url + "-" + action.type;
                }
                if(branch){
                    urlTypeBranch += "-" + branch.id;
                }
                if(urlTypeBranch &&  userActions.indexOf(urlTypeBranch) < 0){
                    userActions.push(urlTypeBranch);
                }
            });
        }
        return userActions;
    }
}