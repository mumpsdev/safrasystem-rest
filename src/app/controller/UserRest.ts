import * as express from "express";
import { URLs } from "../utils/Values";
import UserService from "../service/UserService";

class UserRest{
    constructor(){}
    public setRoutes(exp: express.Application){
        exp.get(URLs.USER_ID, UserService.get);
        exp.get(URLs.USER, UserService.list);
        exp.post(URLs.USER, UserService.create);
        exp.put(URLs.USER_ID, UserService.update);
        exp.delete(URLs.USER_ID, UserService.delete);
    }
}
export default new UserRest();