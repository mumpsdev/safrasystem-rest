import * as express from "express";
import { URLs } from "../../utils/Values";
import ProfileService from "../../service/control-access/ProfileService";

class ProfileRest{
    constructor(){}
    public setRoutes(exp: express.Application){
        exp.get(URLs.PROFILE, ProfileService.list);
        exp.post(URLs.PROFILE, ProfileService.create);
        exp.get(URLs.PROFILE_ID, ProfileService.get);
        exp.put(URLs.PROFILE_ID, ProfileService.update);
        exp.delete(URLs.PROFILE_ID, ProfileService.delete);
    }
}
export default new ProfileRest();