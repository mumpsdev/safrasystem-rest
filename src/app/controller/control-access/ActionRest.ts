import * as express from "express";
import { URLs } from "../../utils/Values";
import ActionService from "../../service/control-access/ActionService";

class ActionRest{
    constructor(){}
    public setRoutes(exp: express.Application){
        exp.get(URLs.ACTION, ActionService.list);
        exp.post(URLs.ACTION, ActionService.create);
        exp.get(URLs.ACTION_ID, ActionService.get);
        exp.put(URLs.ACTION_ID, ActionService.update);
        exp.delete(URLs.ACTION_ID, ActionService.delete);
    }
}
export default new ActionRest();