import * as express from "express";
import { URLs } from "../../utils/Values";
import BranchService from "../../service/control-access/BranchService";

class BranchRest{
    constructor(){}
    public setRoutes(exp: express.Application){
        exp.get(URLs.BRANCH, BranchService.list);
        exp.post(URLs.BRANCH, BranchService.create);
        exp.get(URLs.BRANCH_ID, BranchService.get);
        exp.put(URLs.BRANCH_ID, BranchService.update);
        exp.delete(URLs.BRANCH_ID, BranchService.delete);
    }
}
export default new BranchRest();