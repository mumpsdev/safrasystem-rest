import * as express from "express";
import { URLs } from "../../utils/Values";
import CompanyService from "../../service/control-access/CompanyService";

class CompanyRest{
    constructor(){}
    public setRoutes(exp: express.Application){
        exp.get(URLs.COMPANY, CompanyService.list);
        exp.post(URLs.COMPANY, CompanyService.create);
        exp.get(URLs.COMPANY_ID, CompanyService.get);
        exp.put(URLs.COMPANY_ID, CompanyService.update);
        exp.delete(URLs.COMPANY_ID, CompanyService.delete);
    }
}
export default new CompanyRest();