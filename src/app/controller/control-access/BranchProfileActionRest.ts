import * as express from "express";
import { URLs } from "../../utils/Values";
import branchProfileActionService from "../../service/control-access/branchProfileActionService";

class branchProfileActionRest{
    constructor(){}
    public setRoutes(exp: express.Application){
        exp.get(URLs.BRANCH_PROFILE_ACTION, branchProfileActionService.list);
        exp.post(URLs.BRANCH_PROFILE_ACTION, branchProfileActionService.create);
        exp.get(URLs.BRANCH_PROFILE_ACTION_ID, branchProfileActionService.get);
        exp.put(URLs.BRANCH_PROFILE_ACTION_ID, branchProfileActionService.update);
        exp.delete(URLs.BRANCH_PROFILE_ACTION_ID, branchProfileActionService.delete);
    }
}
export default new branchProfileActionRest();