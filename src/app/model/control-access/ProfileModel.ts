import { Model } from 'sequelize-typescript';
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { Default } from 'sequelize-typescript/lib/annotations/Default';
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Unique } from "sequelize-typescript/lib/annotations/Unique";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { BranchProfileActionModel } from "./BranchProfileActionModel";

@Table({tableName:"profile"})
export class ProfileModel extends Model<ProfileModel> {

    @Unique
    @Column({allowNull: false, field: "name", comment : 'Nome do perfil.'})
    name: string;

    @HasMany( () => BranchProfileActionModel)
    BranchProfileActions : BranchProfileActionModel[];

    @Default(true)
    @Column({allowNull: false, field: "active", comment : 'Informa se está ativo ou não.'})
    active: boolean;

    @CreatedAt
    @Column({allowNull: false, field: "date_create", comment : 'Data do cadastro da tabela.'})
    dateCreate: Date;

    @UpdatedAt
    @Column({allowNull: false, field: "date_update", comment : 'Data da atualização da tabela.'})
    dateUpdate: Date;
}