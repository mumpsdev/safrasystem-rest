import { CreatedAt, UpdatedAt } from "sequelize-typescript";
import { BelongsTo } from "sequelize-typescript/lib/annotations/association/BelongsTo";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { ForeignKey } from "sequelize-typescript/lib/annotations/ForeignKey";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { Model } from "sequelize-typescript/lib/models/Model";
import { ActionModel } from "./ActionModel";
import { BranchModel } from "./branchModel";
import { ProfileModel } from "./ProfileModel";

@Table({tableName:"branch_profile_action"})
export class BranchProfileActionModel extends Model<BranchProfileActionModel>{
    static sequelize: any;

    @ForeignKey(() => ProfileModel)
    @Column({field: "profile_id", comment : 'Perfil selecionado.'})
    profileId: number;

    @BelongsTo(() => ProfileModel)
    profile: ProfileModel;
    
    @ForeignKey(() => BranchModel)
    @Column({field: "branch_id", comment : 'Filial do perfil.'})
    branchId: number;

    @BelongsTo(() => BranchModel)
    branch: BranchModel;

    @ForeignKey(() => ActionModel)
    @Column({field: "action_id", comment: "Acão do perfil e da ação."})
    actionId: number;

    @BelongsTo(() => ActionModel)
    action: ActionModel;

    @CreatedAt
    @Column({allowNull: false, field: "date_create", comment : 'Data do cadastro da tabela.'})
    dateCreate: Date;

    @UpdatedAt
    @Column({allowNull: false, field: "date_update", comment : 'Data da atualização da tabela.'})
    dateUpdate: Date;
}