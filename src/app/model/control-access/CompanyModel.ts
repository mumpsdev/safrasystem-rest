import { Column, Model, Table } from "sequelize-typescript";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { Default } from "sequelize-typescript/lib/annotations/Default";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { BranchModel } from "./branchModel";


@Table({tableName:"company"})
export class CompanyModel extends Model<CompanyModel>{

    @Column({allowNull: true, field: "cnpj", comment : 'CNPJ fantasia da organização.'})
    cnpj: string;

    @Column({allowNull: false, field: "fantasy", comment : 'Nome fantasia da organização.'})
    fantasy: string;
    
    @Column({allowNull: false, field: "social_reason", comment : 'Razão Social da organização.'})
    socialReason: string;

    @CreatedAt
    @Column({allowNull: false, field: "date_create", comment : 'Data do cadastro da tabela.'})
    dateCreate: Date;

    @UpdatedAt
    @Column({allowNull: false, field: "date_update", comment : 'Data da atualização da tabela.'})
    dateUpdate: Date;

    @Default("2")
    @Column({
        values: ['1', '2', '3', '4'],
        allowNull: false, 
        field: "status", 
        comment : '1 -> Pendente, 2 -> ativo, 3 -> suspenso e 4 -> removido'
    })
    status: number
    
    @HasMany( () => BranchModel)
    branchs : BranchModel[]
}