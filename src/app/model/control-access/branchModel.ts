import { BelongsTo, Column, ForeignKey, Model, Table, HasMany } from "sequelize-typescript";
import { CreatedAt } from "sequelize-typescript/lib/annotations/CreatedAt";
import { Default } from "sequelize-typescript/lib/annotations/Default";
import { UpdatedAt } from "sequelize-typescript/lib/annotations/UpdatedAt";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { CompanyModel } from "./CompanyModel";
import { BranchProfileActionModel } from "./BranchProfileActionModel";

@Table({tableName:"branch"})
export class BranchModel extends Model<BranchModel>{

    @Column({allowNull: true, field: "cnpj", comment : 'CNPJ fantasia da filial.'})
    cnpj: string;

    @Column({allowNull: false, field: "fantasy", comment : 'Nome fantasia da filial.'})
    fantasy: string;
    
    @Column({allowNull: false, field: "social_reason", comment : 'Razão Social da filial.'})
    socialReason: string;

    
    @ForeignKey(() => CompanyModel)
    @Column({allowNull: false, field: "company_id", comment : 'Organização selecionada para a filial.'})
    companyId: number;
    
    @BelongsTo(() => CompanyModel)
    company: CompanyModel;

    @HasMany( () => BranchProfileActionModel)
    branchProfileActions : BranchProfileActionModel[]
    
    @Default("2")
    @Column({
        values: ['1', '2', '3', '4'],
        allowNull: false, 
        field: "status", 
        comment : '1 -> Pendente, 2 -> ativo, 3 -> suspenso e 4 -> removido'
    })
    status: number
    
    @CreatedAt
    @Column({allowNull: false, field: "date_create", comment : 'Data do cadastro da tabela.'})
    dateCreate: Date;

    @UpdatedAt
    @Column({allowNull: false, field: "date_update", comment : 'Data da atualização da tabela.'})
    dateUpdate: Date;
}