import { CreatedAt, UpdatedAt } from "sequelize-typescript";
import { HasMany } from "sequelize-typescript/lib/annotations/association/HasMany";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { DataType } from "sequelize-typescript/lib/enums/DataType";
import { Model } from "sequelize-typescript/lib/models/Model";
import { BranchProfileActionModel } from "./BranchProfileActionModel";
// { "name": "Tipo de contato 3", "group": "Cadastro", "router": "/contact-type", "icon": "delete" },
@Table({tableName:"action"})
export class ActionModel extends Model<ActionModel>{
    static sequelize: any;

    @Column({allowNull: true, field: "menu_label", comment : 'Nome da ação.', defaultValue: null})
    menuLabel: string;

    @Column({allowNull: true, field: "router", comment : 'Rota da ação.'})
    router: string;

    @Column({allowNull: true, field: "url", comment : 'Url da ação.'})
    url: string;

    @Column({allowNull: true, field: "name", comment : 'Acão executada da ação.'})
    name: string;
    
    @Column({allowNull: true, field: "group", comment : 'Grupo da ação.'})
    group: string;
    
    @Column({allowNull: true, field: "icon", comment : 'Icon da ação.'})
    icon: string;
    
    @HasMany( () => BranchProfileActionModel)
    branchProfileActions : BranchProfileActionModel[]
    
    @Column({
        type : DataType.ENUM,
        values: [ 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH', 'NONE' ],
        allowNull: false, 
        field: "method"
    })
    method: string

    @CreatedAt
    @Column({allowNull: false, field: "date_create", comment : 'Data do cadastro da tabela.'})
    dateCreate: Date;

    @UpdatedAt
    @Column({allowNull: false, field: "date_update", comment : 'Data da atualização da tabela.'})
    dateUpdate: Date;
}