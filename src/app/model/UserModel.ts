import { Column, CreatedAt, Default, HasMany, Model, Table, UpdatedAt, ForeignKey, BelongsTo } from "sequelize-typescript";
import { ProfileModel } from "./control-access/ProfileModel";


@Table({tableName:"user"})
export class UserModel extends Model<UserModel>{

    @Column({allowNull: false, field: "name", comment : 'Nome do usuário.'})
    name: string

    @Column({allowNull: false, field: "login", comment : 'Login de acesso Web.'})
    login: string

    @Column({allowNull: false, field: "password", comment : 'Password do Colaborador.'})
    password: string

    @Column({defaultValue: true, allowNull: false, field: "active", comment : 'Informa se está ativo ou não.'})
    active: boolean
    
    //Flag que define super usuário ou desenvolvedor.
    @Column({defaultValue: false, field: "is_admin", comment : 'Informa se o colaborador é administrator do sitema.'})
    isAdmin: boolean

    //Flag que define se é administrador ou dono da empresa.
    @Column({defaultValue: false, field: "is_owner", comment : 'Informa se o colaborador é administrator ou dono da empresa.'})
    isOwner: boolean

    @ForeignKey(() => ProfileModel)
    @Column({allowNull: true, field: "profile_id", comment : 'Perfil selecionado.'})
    profileId: number;

    @BelongsTo(() => ProfileModel)
    profile: ProfileModel;

    @CreatedAt
    @Column({allowNull: false, field: "date_create", comment : 'Data do cadastro da tabela.'})
    dateCreate: Date

    @UpdatedAt
    @Column({allowNull: false, field: "date_update", comment : 'Data da atualização da tabela.'})
    dateUpdate: Date
}