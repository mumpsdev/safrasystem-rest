export const msgGeneric = {
    "notRecordsFound"           : "NOT_RECORDS_FOUND",
    "errorFindRecord"           : "ERROR_FIND_RECORD",
    "errorListRecords"          : "ERROR_LIST_RECORDS",
    "errorInsertRecord"         : "ERROR_INSERT_RECORD",
    "errorAlterRecord"          : "ERROR_ALTER_RECORD",
    "recordInsertedSuccessfully": "RECORD_INSERTED_SUCCESSFULLY",
    "recordAlteredSuccessfully" : "RECORD_ALTERED_SUCCESSFULLY",
    "recordRemovedSuccessfully" : "RECORD_REMOVED_SUCCESSFULLY",
    "notSetField"               : "NOT_SET_FIELD",
    "recordAlreadyExists"       : "RECORD_ALREADY_EXISTS",
    "recordNotExist"            : "RECORD_NOT_EXIST",
    "idNotSet"                  : "ID_NOT_SET",
    "cnpjInvalid"               : "CNPJ_INVALID",
}

export const msgAuth = {//Mensagens de retornos
    "errorQueryLoginAccess"       : "ERROR_QUERY_LOGIN_ACCESS",
    "notSendToken"                : "NOT_SEND_TOKEN",
    "loginInvalid"                : "LOGIN_INVALID",
    "tokenInvalid"                : "TOKEN_INVALID",
    "loginPasswordInvalid"        : "LOGIN_PASSWORD_INVALID",
    "loginRequired"               : "LOGIN_REQUiRED",
    "passwordRequired"            : "PASSWORD_REQUiRED",
    "errorValidToken"             : "ERROR_VALID_TOKEN",
    "notLoginFound"               : "NOT_LOGIN_FOUND",
    "acessDanied"                 : "ACESS_DANIED",
    "tokenGenerateSuccessfully"   : "TOKEN_GENERATE_SUCCESSFULLY"
};

export const msgUser = {
    "nameUserRequired"           : "NAME_USER_REQUIRED",
    "loginUserRequired"          : "LOGIN_USER_REQUIRED",
    "passwordUserRequired"       : "PASSWORD_USER_REQUIRED",
    "loginAlreadyRegistered"     : "LOGIN_ALREADY_REGISTERED"
}

export const msgCompany = {
    "fantasyCompanyRequired"           : "FANTASY_COMPANY_REQUIRED",
    "socialReasonCompanyRequired"      : "SOCIAL_REASON_COMPANY_REQUIRED",
    "companyNotFound"                  : "COMPANY_NOT_FOUND",
    "companyNotExist"                  : "COMPANY_NOT_EXIST",
}

export const msgBranch = {
    "fantasyBranchRequired"           : "FANTASY_BRANCH_REQUIRED",
    "branchRequired"                  : "BRANCH_REQUIRED",
    "socialReasonBranchRequired"      : "SOCIAL_REASON_BRANC_REQUIRED",
    "branchNotExist"                  : "BRANCH_NOT_EXIST",
}

export const msgAction = {
    "methodActionRequired"                 : "METHOD_ACTION_REQUIRED",
    "actionRequired"                       : "ACTION_REQUIRED",
    "nameOrUrlOrRouterActionRequired"      : "NAME_OR_URL_OR_ROUTER_ACTION_REQUIRED",
    "actionNotExist"                       : "ACTION_NOT_EXIST",
}

export const msgProfile = {
    "nameProfileRequired"           : "NAME_PROFILE_REQUIRED",
    "profileRequired"               : "PROFILE_REQUIRED",
    "profileNotExist"            : "PROFILE_NOT_EXIST"
}