import * as http from "http";
import App from "./App"
import { FactoryDB } from "./databases/FactoryDB";
import { UserModel } from "./model/UserModel";
import UtilService from "./service/UtilService";

const port:number = normalizePort(parseInt(process.env.PORT) || 3000);
// const port:number = normalizePort(8080);

App.exp.set('port', port);

const server = http.createServer(App.exp);

let factoryDB: FactoryDB = new FactoryDB();


server.listen(port, "0.0.0.0", () =>{
    try {
        console.log("Servidor está rodando na porta "+ port);
        factoryDB.createConnectionSequelize( ()=> {
                console.log("Database connected with success!");
                factoryDB.updateTableSequelize( () => {
                    console.log("Database atualizado com sucesso with success!");
                    // let password = UtilService.encrypt("123456");
                    // let user =  {"name": "Testador do sistema", "login": "mumps",  "password": password};
                    // UserModel.create(user);
                    // console.log("Usuário criado com sucesso!");
                    
                }, (error) => {
                    console.log("Erro ao tentar atualizar banco de dados!");
                    console.log(error);
                });
        }, (error) => {
                console.log("Error trying to connect to database: " + error);
        })
    } catch (error) {
        console.log(error);
    }
});

server.on('error', onError);

// server.on('listening', onListening);

process.once("SIGUSR2", () => {
    factoryDB.closeConnectionSequelize(() => {
         console.log("System reboot!");
         process.kill(process.pid, "SIGUSR2");
    });
});

process.once("SIGINT", () => {
    factoryDB.closeConnectionSequelize(() => {
       console.log("Sistema fechado!");
       process.exit(0);
    });
});

function normalizePort(val: number): number {
    let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port)) return val;
    else if (port >= 0) return port;
    else return port;
}

function onError(error: NodeJS.ErrnoException): void {
  if (error.syscall !== 'listen') throw error;
  let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;
  switch(error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

// function onListening(): void {
//   let addr = server.address();
//   let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
//   debug(`Listening on ${bind}`);
//   debug(`Listening on teste`);
// }
